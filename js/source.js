var module = {},
    apiData;

function filterData(data, key, value) {
    var filteredData = data.filter(function(item) {
        return value == item[key];
    });

    var filterObj = {};
    filterObj.transactions = filteredData;
    return filterObj;

}

function filterNow () {
    var searchType = document.getElementById('searchType').value;
    var searchValue = document.getElementById('searchValue').value;

    if (searchType && searchValue) {
        var dataTransactions = apiData.transactions;
        var filteredData = filterData(dataTransactions, searchType, searchValue);

        var tableHeaderRowCount = 1;
        var table = document.getElementById('myTable');
        var rowCount = table.rows.length;
        for (var i = tableHeaderRowCount; i < rowCount; i++) {
            table.deleteRow(tableHeaderRowCount);
        }

        populateData(filteredData);
    } else {
        populateData(apiData);
    }
}

function displayAverage () {
    var accountName = document.getElementById('accountName').value;
    if (accountName) {
        var average = getAverage(accountName);
        document.getElementById('accountAverage').innerHTML = ''+average;
    } else {
        document.getElementById('accountAverage').innerHTML = 0;
    }
}

function getAverage(account) {
    var sum = 0;
    var filteredList = apiData.transactions.filter(function (item) {
        return item._account === account;
    });
    var filterByDate = filteredList.sort(function (prev, next) {
        return +new Date(prev.date) > +new Date(next.date)
    });
    filterByDate.map(function (transaction) {
        return sum += transaction.amount;
    });
    var reqDate = monthDiff(filteredList[0].date, filteredList[filteredList.length - 1].date);
    return (sum/reqDate).toFixed(3);
}

function monthDiff(from, to) {
    var month = 1;
    var dateFrom = new Date(from);
    var dateTo = new Date(to);

    month = (dateTo.getMonth() - dateFrom.getMonth()) + (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));
    if (month == 1 && dateFrom < dateTo) month = 2;
    return month;
}

function loadData(apiUrl) {

    var xhr = new XMLHttpRequest(),
        //url= 'https://autofi-sim.herokuapp.com/interview',
        url= 'json/data.json',
        data= {};

    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {

            apiData =  JSON.parse(xhr.responseText);

            populateData(apiData);

        }
    };

    xhr.open("GET", url, true);
    xhr.send();

}

function populateData (data) {
    var table = document.getElementById('myTable');

    data.transactions.map(function(item) {

        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        cell1.innerHTML = item._account;
        cell2.innerHTML = item.name;
        cell3.innerHTML = '$'+item.amount;
        cell4.innerHTML = item.date;

    });
}

loadData();
