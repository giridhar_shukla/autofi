#Autofi
This app is used to display the account transaction list details like their amount, date and other data.

##Features
 - Display account data in tabular form
 - Filter list based on Account Id or name
 - Get average of accounts based on account id
  

## How to start the app/
 - Need any local server to run the app like node server.
 - Install http-server using npm install -g http-server.
 - Unzip the file and go to the project folder.
 - Run 'http-server' in terminal, this will start local server. You can give port number as well.
 - Run localhost in the browser and use the app.


